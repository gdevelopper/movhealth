<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VideoRepository")
 */
class Video
{
    function __construct()
    {
        $this->created_at = new \DateTime();
        $this->videoComments = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo5;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo6;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo7;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo8;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo9;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo10;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VideoComment", mappedBy="video", orphanRemoval=true)
     */
    private $videoComments;

    public function getSlug():string{
        return (new Slugify())->slugify($this->name);
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getPhoto1(): ?string
    {
        return $this->photo1;
    }

    public function setPhoto1(?string $photo1): self
    {
        $this->photo1 = $photo1;

        return $this;
    }

    public function getPhoto2(): ?string
    {
        return $this->photo2;
    }

    public function setPhoto2(?string $photo2): self
    {
        $this->photo2 = $photo2;

        return $this;
    }

    public function getPhoto3(): ?string
    {
        return $this->photo3;
    }

    public function setPhoto3(?string $photo3): self
    {
        $this->photo3 = $photo3;

        return $this;
    }

    public function getPhoto4(): ?string
    {
        return $this->photo4;
    }

    public function setPhoto4(?string $photo4): self
    {
        $this->photo4 = $photo4;

        return $this;
    }

    public function getPhoto5(): ?string
    {
        return $this->photo5;
    }

    public function setPhoto5(?string $photo5): self
    {
        $this->photo5 = $photo5;

        return $this;
    }

    public function getPhoto6(): ?string
    {
        return $this->photo6;
    }

    public function setPhoto6(?string $photo6): self
    {
        $this->photo6 = $photo6;

        return $this;
    }

    public function getPhoto7(): ?string
    {
        return $this->photo7;
    }

    public function setPhoto7(?string $photo7): self
    {
        $this->photo7 = $photo7;

        return $this;
    }

    public function getPhoto8(): ?string
    {
        return $this->photo8;
    }

    public function setPhoto8(?string $photo8): self
    {
        $this->photo8 = $photo8;

        return $this;
    }

    public function getPhoto9(): ?string
    {
        return $this->photo9;
    }

    public function setPhoto9(?string $photo9): self
    {
        $this->photo9 = $photo9;

        return $this;
    }

    public function getPhoto10(): ?string
    {
        return $this->photo10;
    }

    public function setPhoto10(?string $photo10): self
    {
        $this->photo10 = $photo10;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|VideoComment[]
     */
    public function getVideoComments(): Collection
    {
        return $this->videoComments;
    }

    public function addVideoComment(VideoComment $videoComment): self
    {
        if (!$this->videoComments->contains($videoComment)) {
            $this->videoComments[] = $videoComment;
            $videoComment->setVideo($this);
        }

        return $this;
    }

    public function removeVideoComment(VideoComment $videoComment): self
    {
        if ($this->videoComments->contains($videoComment)) {
            $this->videoComments->removeElement($videoComment);
            // set the owning side to null (unless already changed)
            if ($videoComment->getVideo() === $this) {
                $videoComment->setVideo(null);
            }
        }

        return $this;
    }
}
