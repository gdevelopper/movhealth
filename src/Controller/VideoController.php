<?php
/**
 * Created by PhpStorm.
 * User: gdeveloppeur
 * Date: 23/12/18
 * Time: 16:11
 */

namespace App\Controller;


use App\Entity\Filter;
use App\Entity\User;
use App\Entity\Video;
use App\Entity\VideoComment;
use App\Form\FilterFormType;
use App\Form\VideoCommentType;
use App\Repository\VideoCommentRepository;
use App\Repository\VideoRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VideoController extends AbstractController
{
    /**
     * @Route("/video",name="video")
     * @return Response
     */
    function index (VideoRepository $repo,PaginatorInterface $paginator,Request $request):Response{
        $filter = new Filter();
        $form = $this->createForm(FilterFormType::class,$filter);
        $form->handleRequest($request);
        if(($submit  = ($form->isSubmitted() && $form->isValid()))){
            $videos = $repo->findByFilter($filter);
        }else{
            $videos = $repo->findAll();
            $videos = $paginator->paginate($videos, $request->query->getInt('page',1), 10);
        }


        return $this->render("myPages/Video.twig",["current"=>"video","videos"=>$videos,"filter"=>$submit,"form"=>$form->createView()]);
    }

    /**
     * @Route("/video/{slug}-{id}",name="videoDetail",requirements={"slug":"[-a-zA-Z0-9]+"},options={"expose"=true})
     * @return Response
     */
    function videoDetail($slug,$id,VideoRepository $repo,Request $req,VideoCommentRepository $vcRepo):Response{
        $video = $repo->find($id);
        $comments = $vcRepo->findOrderCommentOf($video);
        $smVideos  = $repo->findThreeLatestVideo();

        $user = $this->getUser();
        $comment  =  new VideoComment();
        $form =  $this->createForm(VideoCommentType::class,$comment);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid() && $req->isXmlHttpRequest()){
            $comment->setUser($user);
            $comment->setVideo($video);
            $manager  = $this->getDoctrine()->getManager();
            $manager->persist($comment);
            $manager->flush();
            $reponse  = new JsonResponse(["comment"=>    $comment->getComment(),
                "created_at"=> $comment->getCreatedAt(),
                "userFN"=>     $user->getFirstName(),
                "userLN"=>     $user->getLastName()
            ]);
            $reponse->headers->set("Content-Type","application/json");
            return $reponse;
        }

        return $this->render("myPages/VideoDetail.html.twig",["current"=>"video",
                                                          "form"=>$form->createView(),
                                                          "video"=>$video,
                                                          "comments"=>$comments,
                                                          "smVideos"=>$smVideos]);
    }
}