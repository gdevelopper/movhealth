<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TopicalityRepository")
 */
class Topicality
{
    function __construct()
    {
        $this->created_at = new \DateTime();
        $this->topicComments = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $topic_title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $topic_owner;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $topic_text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TopicComment", mappedBy="topic", orphanRemoval=true)
     */
    private $topicComments;

    public function getSlug():string{
        return (new Slugify())->slugify($this->topic_title);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTopicTitle(): ?string
    {
        return $this->topic_title;
    }

    public function setTopicTitle(string $topic_title): self
    {
        $this->topic_title = $topic_title;

        return $this;
    }

    public function getTopicOwner(): ?string
    {
        return $this->topic_owner;
    }

    public function setTopicOwner(string $topic_owner): self
    {
        $this->topic_owner = $topic_owner;

        return $this;
    }

    public function getTopicText(): ?string
    {
        return $this->topic_text;
    }

    public function setTopicText(string $topic_text): self
    {
        $this->topic_text = $topic_text;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|TopicComment[]
     */
    public function getTopicComments(): Collection
    {
        return $this->topicComments;
    }

    public function addTopicComment(TopicComment $topicComment): self
    {
        if (!$this->topicComments->contains($topicComment)) {
            $this->topicComments[] = $topicComment;
            $topicComment->setTopic($this);
        }

        return $this;
    }

    public function removeTopicComment(TopicComment $topicComment): self
    {
        if ($this->topicComments->contains($topicComment)) {
            $this->topicComments->removeElement($topicComment);
            // set the owning side to null (unless already changed)
            if ($topicComment->getTopic() === $this) {
                $topicComment->setTopic(null);
            }
        }

        return $this;
    }
}
