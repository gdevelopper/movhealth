<?php
/**
 * Created by PhpStorm.
 * User: gdeveloppeur
 * Date: 27/12/18
 * Time: 22:20
 */

namespace App\Controller;


use App\Entity\User;
use App\Form\UserFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class InscriptionController extends AbstractController
{
    /**
     * @Route("/inscription",name="inscription")
     * @return Response
     */
    function index(Request $request,EntityManagerInterface $manager,UserPasswordEncoderInterface $encoder):Response{

        $user = new User();
        $form = $this->createForm(UserFormType::class,$user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($user);
            $user->setPassword($encoder->encodePassword($user,$user->getPassword()));
            $manager->flush();
            return $this->redirectToRoute("accueil",["current"=>"video","message"=>"inscription réussie"],301);
        }


        return $this->render("myPages/Inscription.twig",["current"=>"inscription","form"=>$form->createView()]);
    }
}