<?php

namespace App\Repository;

use App\Entity\Topicality;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Topicality|null find($id, $lockMode = null, $lockVersion = null)
 * @method Topicality|null findOneBy(array $criteria, array $orderBy = null)
 * @method Topicality[]    findAll()
 * @method Topicality[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TopicalityRepository extends ServiceEntityRepository
{
    private $year;
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Topicality::class);
        $this->year = "YEAR";
    }

    public function findByFilter($filter)
    {
        // ici je vais creer le prédicat

        $where = " ";
        $parms = [];
        $and = false;
        if($filter->getKeyWords()){
            $where = $where . "p.topic_text like :val0";
            $parms["val0"] = '%'.$filter->getKeyWords()[0].'%';
            for($i=1;$i<count($filter->getKeyWords());$i++){
                $parms["val$i"] ="%".$filter->getKeyWords()[$i]."%";
                $where = $where . "or p.topic_text like :val$i";
            }
            $and = true;
        }
        if($filter->getDate()){
            $where = $and ? $where ." and " : $where;
            $parms["date"] = $filter->getDate();
            $where  = $where . $this->year."(p.created_at) = ".$this->year."(:date)";
        }
        if($filter->getSpecialite()){
            dump("la specialité n'est pas encore implémenté");
        }

        if(count($parms) != 0) {
            return $this->createQueryBuilder("p")
                ->andWhere($where)
                ->setParameters($parms)
                ->getQuery()
                ->getResult();
        }else{
            return $this->find(0);
        }
    }
    // /**
    //  * @return Topicality[] Returns an array of Topicality objects
    //  */

    public function findThreeLatestTopic()
    {
        return $this->createQueryBuilder('v')
            ->orderBy('v.created_at', 'ASC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
            ;
    }


    /*
    public function findOneBySomeField($value): ?Topicality
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
