<?php
/**
 * Created by PhpStorm.
 * User: gdeveloppeur
 * Date: 01/01/19
 * Time: 19:07
 */

namespace App\Entity;


class Filter
{
    private $specialite ;
    private $keyWords ;
    private $date;

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getKeyWords()
    {
        return $this->keyWords;
    }

    /**
     * @param mixed $keyWords
     */
    public function setKeyWords($keyWords): void
    {
        $this->keyWords = explode(" ",$keyWords);
    }

    /**
     * @return mixed
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * @param mixed $specialite
     */
    public function setSpecialite($specialite): void
    {
        $this->specialite = $specialite;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $d = new \DateTime();
        $d->setDate($date+0,12,31);
        $this->date = $d;
    }

}