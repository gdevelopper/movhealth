<?php
/**
 * Created by PhpStorm.
 * User: gdeveloppeur
 * Date: 27/12/18
 * Time: 23:20
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthentificationController extends AbstractController
{
    /**
     * @Route("/login",name="login")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function login(AuthenticationUtils $utils):Response{
        return $this->render("myPages/connexion.twig",["current"=>"login","last_user"=>$utils->getLastUsername(),"error"=>$utils->getLastAuthenticationError()]);
    }

}