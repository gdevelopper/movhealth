<?php
/**
 * Created by PhpStorm.
 * User: gdeveloppeur
 * Date: 23/12/18
 * Time: 14:20
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AproposControler extends AbstractController
{
    /**
     *  Action par defaut pour la page Apropos
     * @Route("/apropos",name="apropos")
     */
        function index ():Response
        {
            return $this->render("myPages/Apropos.twig",["current"=>"apropos"]);
        }
}