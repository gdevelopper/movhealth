<?php

namespace App\Repository;

use App\Entity\Video;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository
{
    private $year ;
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Video::class);
        $this->year = "YEAR";
    }

    public function findByFilter($filter){

        $where = " ";
        $parms = [];
        $and = false;
        if($filter->getKeyWords()){
            $where = $where . "p.description like :val0";
            $parms["val0"] = '%'.$filter->getKeyWords()[0].'%';
            for($i=1;$i<count($filter->getKeyWords());$i++){
                 $parms["val$i"] ="%".$filter->getKeyWords()[$i]."%";
                 $where = $where . "or p.description like :val$i";
            }
            $and = true;
        }
        if($filter->getDate()){
            $where = $and ? $where ." and " : $where;
            $parms["date"] = $filter->getDate();
            $where  = $where . $this->year."(p.created_at) = ".$this->year."(:date)";
        }
        if($filter->getSpecialite()){
            dump("la specialité n'est pas encore implémenté");
        }

        if(count($parms) != 0) {
            return $this->createQueryBuilder("p")
                ->andWhere($where)
                ->setParameters($parms)
                ->getQuery()
                ->getResult();
        }else{
            return $this->find(0);
        }
    }

    // /**
    //  * @return Video[] Returns an array of Video objects
    //  *//*
    public function findThreeLatestVideo()
    {
        return $this->createQueryBuilder('v')
            ->orderBy('v.created_at', 'ASC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?Video
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
