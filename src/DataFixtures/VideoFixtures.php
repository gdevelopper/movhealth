<?php

namespace App\DataFixtures;

use App\Entity\Video;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;



class VideoFixtures extends Fixture
{
    private $videoUrl = [ "https://www.youtube.com/embed/aRhzKvl86Ag",
                          "https://www.youtube.com/embed/7wCEsajNRo0",
                          "https://www.youtube.com/embed/VXwkNapq7c0",
                          "https://www.youtube.com/embed/727Q1vrFHh4",
                          "https://www.youtube.com/embed/GvAuuS1Expc",
                          "https://www.youtube.com/embed/LY0YLQbRYCw",
                            "https://www.youtube.com/embed/m4GEJeVZwPU",
                            "https://www.youtube.com/embed/DhNHGleHjrg",
                            "https://www.youtube.com/embed/KV1jBeg-tpU",
                            "https://www.youtube.com/embed/yTOz4J-H7EE"];

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");

        for($i=0;$i<100;$i++){
            $video = new Video();
            $video->setDescription($faker->paragraphs($nb=20,$asText=true))
                ->setName($faker->words($nb=3,$asText=true))
                ->setPath($this->videoUrl[rand(0,9)])
                ->setPhoto1($faker->imageUrl($width=800,$height=600))
                ->setPhoto2($faker->imageUrl($width=800,$height=600))
                ->setPhoto3($faker->imageUrl($width=800,$height=600))
                ->setPhoto4($faker->imageUrl($width=800,$height=600))
                ->setPhoto5($faker->imageUrl($width=800,$height=600))
                ->setPhoto6($faker->imageUrl($width=800,$height=600))
                ->setPhoto7($faker->imageUrl($width=800,$height=600))
                ->setPhoto8($faker->imageUrl($width=800,$height=600))
                ->setPhoto9($faker->imageUrl($width=800,$height=600))
                ->setPhoto10($faker->imageUrl($width=800,$height=600))
            ;
            $manager->persist($video);
        }

        $manager->flush();
    }
}
