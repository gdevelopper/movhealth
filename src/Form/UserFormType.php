<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class UserFormType extends AbstractType
{
    private $fist_name_constraints ;
    private $last_name_constraints ;
    private $country_constraints;
    private $phone_number_constraints;
    private $email_constraints;
    private $postal_code_constraints;
    private $speciality_constraints;
    private $password_constraints ;
    private $no_order_constraints;
    /**
     * UserFormType constructor qui me sert a deffinir les contraints de vamidation du formulaire
     */
    function __construct()
    {
        $this->fist_name_constraints = [new Regex([ 'pattern'=>"/^[a-z][-a-z]*$/i",
                                                    'htmlPattern'=>'[a-zA-Z][-a-zA-Z]*$',
                                                    'message'=>"Votre prénom n'est pas valide"]),
                                        new NotBlank(["message"=>"Veillez a remplir ce schamp"])
        ];

        $this->last_name_constraints = [new Regex(["pattern"=>"/^[a-zA-Z][-a-zA-Z]*$/",
                                                        "message"=>"votre nom  n'est pas valide"])
                                            ,new NotBlank(["message"=>"Veillez a remplir ce schamp"])
        ];

        $this->country_constraints = [new NotBlank(["message"=>"Veillez a remplir ce schamp"])];

        $this->phone_number_constraints = [new NotBlank(["message"=>"Veillez a remplir ce schamp"]),
                                            new Regex(["pattern"=>"/^0(0[0-9]{2,3})?[0-9]{9}$/",
                                                        "message"=> "Votre numéro est invalide"])];

        $this->email_constraints = [new Regex(["pattern"=>"/^[-\.a-zA-Z0-9]+@[-\.a-zA-Z0-9]+$/",
                                                "message"=>"Votre email {{value}} n'est pas valide"]),
                                    new NotBlank(["message"=>"Veillez a remplir ce schamp"])];
        $this->postal_code_constraints = [new Length(["min" => 5, "max" => 5, "exactMessage"=>"le code postal est constitué de {{ limit }} chiffre"]),
                                          new NotBlank(["message"=>"Veillez a remplir ce schamp"])];
        $this->speciality_constraints = [new NotBlank(["message"=>"Veillez a remplir ce schamp"])];
        $this->password_constraints = [new NotBlank(["message"=>"Veillez a remplir ce schamp"])];
        $this->no_order_constraints = [new NotBlank(["message"=>"Veillez a remplir ce schamp"])];
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', TextType::class,["required"=> true, "constraints"=> $this->fist_name_constraints])
            ->add('last_name',TextType::class,["required"=>true,"constraints"=> $this->last_name_constraints])
            ->add('country',CountryType::class,array("required"=>true,"constraints"=>$this->country_constraints))
            ->add('phone_number',TelType::class,array("required"=>true,"constraints"=>$this->phone_number_constraints))
            ->add('email',EmailType::class,array("required"=>true,"constraints"=>$this->email_constraints))
            ->add('postal_code',NumberType::class,array("required"=>true,"constraints"=>$this->postal_code_constraints))
            ->add('speciality',TextType::class,array("required"=>true,"constraints"=>$this->speciality_constraints))
            ->add("no_order",TextType::class,["required"=>true,"constraints"=>$this->no_order_constraints])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            "translation_domain"=> "form"
        ]);
    }
}
