<?php
/**
 * Created by PhpStorm.
 * User: gdeveloppeur
 * Date: 23/12/18
 * Time: 16:00
 */

namespace App\Controller;


use App\Entity\TopicComment;
use App\Form\FilterFormType;
use App\Entity\Filter;
use App\Form\TopicCommentType;
use App\Repository\TopicalityRepository;
use App\Repository\VideoRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class ActualiteController extends AbstractController
{
    /**
     * @Route("/actualite",name="actualite")
     * @return Response
     */
    function index (TopicalityRepository $repo,Request $request,PaginatorInterface $paginator):Response {
        $filtre = new Filter();
        $form = $this->createForm(FilterFormType::class,$filtre);
        $form->handleRequest($request);
        $search = false;
        if($form->isSubmitted()&& $form->isValid() ){
            $search = true;
            $topics = $repo->findByFilter($filtre);
        }else{
            $topics = $repo->findAll();
            $topics = $paginator->paginate($topics,$request->query->getInt('page',1),10);
        }



        return $this->render("myPages/Actualite.twig",["search"=>$search,"current"=>"actualite","topics"=> $topics,"form"=>$form->createView()]);
    }

    /**
     * @Route("/actualite/{slug}-{id}",name="actualiteDetail",requirements={"slug":"[-a-zA-Z0-9]+"})
     * @return Response
     */
    function actualiteDetial($slug,$id,TopicalityRepository $repo,Request $req,VideoRepository $vr):Response{
        $topic = $repo->find($id);
        $comments = $topic->getTopicComments();
        $video = $vr->findThreeLatestVideo();
        $topics = $repo->findThreeLatestTopic();
        $comment  = new TopicComment();
        $form = $this->createForm(TopicCommentType::class,$comment);
        $form->handleRequest($req);
        if($form->isSubmitted() && $form->isValid()){
            $comment->setUser($this->getUser());
            $comment->setTopic($topic);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($comment);
            $manager->flush();
            return new JsonResponse(["userFN"=>$this->getUser()->getFirstName(),
                                     "userLN"=>$this->getUser()->getLastName(),
                                     "comment"=>$comment->getComment(),
                                     "created_at"=>$comment->getCreatedAt()]);
        }
        return $this->render("myPages/ActualiteDetail.html.twig",["current"=>"actualite",
                                                              "topic"=>$topic,
                                                              "form"=>$form->createView(),
                                                              "comments"=>$comments,
                                                              "smVideos"=>$video,
                                                              "topics"=>$topics]);
    }
}