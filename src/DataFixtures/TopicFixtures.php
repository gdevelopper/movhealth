<?php

namespace App\DataFixtures;

use App\Entity\Topicality;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class TopicFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for($i=0;$i <100;$i++){
            $topic = new Topicality();
            $topic->setTopicOwner($faker->name)
                ->setReference(strval($faker->randomNumber()))
                ->setTopicText($faker->paragraphs($nb=20,$asText=true))
                ->setTopicTitle($faker->words($nb=4,$asText=true))
            ;
            $manager->persist($topic);
        }

        $manager->flush();
    }
}
