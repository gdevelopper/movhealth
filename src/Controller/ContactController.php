<?php
/**
 * Created by PhpStorm.
 * User: gdeveloppeur
 * Date: 23/12/18
 * Time: 15:52
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ContactController extends AbstractController
{
    /**
     * @Route("/contact",name="contact")
     * @return Response
     */
    function index ():Response{
        return $this->render("myPages/Contact.twig",["current" => "contact"]);
    }
}