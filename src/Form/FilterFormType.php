<?php

namespace App\Form;

use App\Entity\Filter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class FilterFormType extends AbstractType
{
    private $keywords_constraints ;
    private $spécialité_constraints ;
    private $date_constraints;

    public function __construct()
    {
        $this->keywords_constraints = [new Regex(["pattern"=>"/\w+/",
                                                  "message"=>"Veillez entrer une phrase sans caractéres spéciaux"])];

        $this->spécialité_constraints = [new Regex(["pattern"=>"/\w+/",
                                                    "message"=>"Veillez entrer une phrase sans caractéres spéciaux"])];

        $this->date_constraints = [new Regex(["pattern"=>"/[0-9]{4}/",
                                              "message"=>"Veillez entrez l'année souhaité à quatre chiffres"])];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('keyWords',TextType::class,["required"=> false,"label"=> false,"attr"=>["placeholder"=>"Votre Recherche"], "constraints"=> $this->keywords_constraints])
            ->add("specialite",ChoiceType::class,["placeholder"=>"Spécialité","choices"=>["Pédiatrie"=>"Pédiatrie","Cardiologie"=>"Cardiologie"],"label"=> false,"required"=> false,"constraints"=> $this->spécialité_constraints,"attr"=>["placeholder"=>"Spécialité"]])
            ->add("date",ChoiceType::class,["label"=>false,"placeholder"=>"Année souhaité","required"=>false,"constraints"=>$this->date_constraints,"choices"=>["2018"=>"2018","2019"=>"2019"]])
            ;
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filter::class,
            "translation_domain"=> "form"
        ]);
    }
}
