<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder ;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstName("ghiles")
            ->setPassword($this->encoder->encodePassword($user,"demo"))
            ->setLastName("denane")
            ->setcountry("France")
            ->setEmail("denane.ghiles@geodis.com")
            ->setSpeciality("data science ")
            ->setPostalCode("93300")
            ->setNoOrder("023121654687")
            ->setPhoneNumber("0767306481");
        $manager->persist($user);
        $manager->flush();
    }
}
