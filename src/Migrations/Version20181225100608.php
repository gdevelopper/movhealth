<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181225100608 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE video (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, path VARCHAR(255) NOT NULL, photo1 VARCHAR(255) DEFAULT NULL, photo2 VARCHAR(255) DEFAULT NULL, photo3 VARCHAR(255) DEFAULT NULL, photo4 VARCHAR(255) DEFAULT NULL, photo5 VARCHAR(255) DEFAULT NULL, photo6 VARCHAR(255) DEFAULT NULL, photo7 VARCHAR(255) DEFAULT NULL, photo8 VARCHAR(255) DEFAULT NULL, photo9 VARCHAR(255) DEFAULT NULL, photo10 VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE topic_comment (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, topic_id INT NOT NULL, comment LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_1CDF0FB99D86650F (user_id), INDEX IDX_1CDF0FB9C4773235 (topic_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video_comment (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, video_id INT NOT NULL, comment LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_7199BBC19D86650F (user_id), INDEX IDX_7199BBC1F02697F5 (video_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE topicality (id INT AUTO_INCREMENT NOT NULL, topic_title VARCHAR(255) NOT NULL, topic_owner VARCHAR(255) NOT NULL, topic_text VARCHAR(255) NOT NULL, reference VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, coutry VARCHAR(255) NOT NULL, no_order VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, postal_code VARCHAR(255) NOT NULL, speciality VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE topic_comment ADD CONSTRAINT FK_1CDF0FB99D86650F FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE topic_comment ADD CONSTRAINT FK_1CDF0FB9C4773235 FOREIGN KEY (topic_id) REFERENCES topicality (id)');
        $this->addSql('ALTER TABLE video_comment ADD CONSTRAINT FK_7199BBC19D86650F FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE video_comment ADD CONSTRAINT FK_7199BBC1F02697F5 FOREIGN KEY (video_id) REFERENCES video (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE video_comment DROP FOREIGN KEY FK_7199BBC1F02697F5');
        $this->addSql('ALTER TABLE topic_comment DROP FOREIGN KEY FK_1CDF0FB9C4773235');
        $this->addSql('ALTER TABLE topic_comment DROP FOREIGN KEY FK_1CDF0FB99D86650F');
        $this->addSql('ALTER TABLE video_comment DROP FOREIGN KEY FK_7199BBC19D86650F');
        $this->addSql('DROP TABLE video');
        $this->addSql('DROP TABLE topic_comment');
        $this->addSql('DROP TABLE video_comment');
        $this->addSql('DROP TABLE topicality');
        $this->addSql('DROP TABLE user');
    }
}
