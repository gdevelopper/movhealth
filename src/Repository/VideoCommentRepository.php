<?php

namespace App\Repository;

use App\Entity\VideoComment;
use App\Entity\Video;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method VideoComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method VideoComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method VideoComment[]    findAll()
 * @method VideoComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoCommentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VideoComment::class);
    }

    // /**
    //  * @return VideoComment[] Returns an array of VideoComment objects
    //  */

    public function findOrderCommentOf ($video)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.video = :id')
            ->setParameter('id', $video)
            ->orderBy('v.created_at', 'DESC')
//            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?VideoComment
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
