<?php
/**
 * Created by PhpStorm.
 * User: gdeveloppeur
 * Date: 23/12/18
 * Time: 15:45
 */

namespace App\Controller;


use App\Entity\Topicality;
use App\Entity\TopicComment;
use App\Entity\User;
use App\Entity\Video;
use App\Entity\VideoComment;
use App\Form\UserFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/",name="accueil")
     * @return Response
     */
    function index (EntityManagerInterface $manager,Request $request,UserRepository $userRepo):Response
    {
        // ici je creer l'entite nouvelle qui va être remplis par le formulaire
        $user = new User();
        $form = $this->createForm(UserFormType::class,$user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute("video",["current"=>"video","message"=>"inscription réussie"],301);
        }


        return $this->render("myPages/Accueil.twig",["current"=>"accueil","form"=>$form->createView()]);
    }
}